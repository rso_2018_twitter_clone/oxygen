#!/bin/sh

dir=`dirname $0`

# Terminate if any command fails
set -e

flake8  --config=$dir/setup.cfg $dir
isort --check-only --settings-path=$dir/setup.cfg --recursive $dir

