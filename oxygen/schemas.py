from oxygen import ma
from oxygen.models import User


class UserSchema(ma.ModelSchema):
    class Meta:
        model = User
        fields = ('id', 'display_name', 'email', 'create_date', 'password')
        load_only = ('password',)


user_schema = UserSchema()
users_schema = UserSchema(many=True)
