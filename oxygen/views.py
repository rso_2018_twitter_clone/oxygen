from datetime import datetime

from flask import abort, jsonify, request
from flask_jwt import current_identity, jwt_required
from sqlalchemy.exc import IntegrityError

from oxygen import app
from oxygen.models import User
from oxygen.schemas import user_schema, users_schema


def respond_error(code, data):
    response = jsonify(data)
    response.status_code = code
    abort(response)


@app.route('/health')
def health():
    return jsonify({"alive": True})


@app.route('/users', methods=['GET', 'POST'])
def users():
    if request.method == 'GET':
        q = request.args.get('q')
        u = User.query.all()
        if q:
            u = User.query.filter(User.display_name.contains(q)).all()
        return users_schema.jsonify(u[:4])
    elif request.method == 'POST':
        data = request.json

        errors = user_schema.validate(data)
        if len(errors) > 0:
            respond_error(400, errors)

        u = User(**data)
        u.create_date = datetime.utcnow()
        try:
            u.save()
        except IntegrityError:
            respond_error(400, {'error': 'Unique constraint validation.'})

        return user_schema.jsonify(u)


@app.route('/users/<int:id>', methods=['GET'])
def user(id):
    u = User.query.get(id)
    if u is None:
        abort(404)
    return user_schema.jsonify(u)


@app.route('/users', methods=['PUT'])
@jwt_required()
def edit_user():
    data = request.json
    u = current_identity

    if not ('display_name' in data.keys()):
        abort(404)

    u.display_name = data['display_name']
    u.save()
    return user_schema.jsonify(u)


@app.route('/users/change_password', methods=['PUT'])
@jwt_required()
def change_password():
    data = request.json
    u = current_identity

    if not ('password' in data.keys()):
        abort(404)

    u.password = data['password']
    u.save()
    return user_schema.jsonify(u)


@app.route('/users', methods=['DELETE'])
@jwt_required()
def delete_user():
    u = current_identity
    u.display_name = ""
    u.email = ""
    u.save()
    return jsonify({'message': "User has been deleted"})
