from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_bcrypt import Bcrypt
from flask_migrate import Migrate, upgrade
from flask_jwt import JWT
from flask_cors import CORS

from config import app_config

app = Flask(__name__)
app.config.from_object(app_config['development'])
CORS(app)

db = SQLAlchemy(app)
migrate = Migrate(app, db)
ma = Marshmallow(app)
bcrypt = Bcrypt(app)

with app.app_context():
    upgrade()

from oxygen.models import User  # NOQA

jwt = JWT(app, User.authenticate, User.identity)

import oxygen.errors  # NOQA
import oxygen.views  # NOQA
