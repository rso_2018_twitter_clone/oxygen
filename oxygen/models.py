from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.hybrid import hybrid_property

from oxygen import bcrypt, db


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    display_name = db.Column(db.String(80), nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    _password = db.Column(db.String(128))
    create_date = db.Column(db.DateTime)

    def __str__(self):
        return 'User {0}'.format(self.email)

    def save(self):
        db.session.add(self)
        try:
            db.session.commit()
        except IntegrityError as e:
            db.session.rollback()
            raise e

    @staticmethod
    def save_all(*objs):
        db.session.add_all(objs)
        db.session.commit()

    @hybrid_property
    def password(self):
        return self._password

    @password.setter
    def password(self, plaintext):
        self._password = bcrypt.generate_password_hash(plaintext).decode('utf-8')

    def check_password(self, password):
        return bcrypt.check_password_hash(self._password, password)

    @staticmethod
    def authenticate(username, password):
        user = User.query.filter_by(email=username).first()
        if user and user.check_password(password):
            return user

    @staticmethod
    def identity(payload):
        user_id = payload['identity']
        return User.query.get(user_id)
