from datetime import timedelta
import logging
import os


class Config:
    """ Base config. """
    DEBUG = False

    SQLALCHEMY_DATABASE_URI = os.getenv('SQLALCHEMY_DATABASE_URI', 'sqlite:////tmp/oxygen.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    JWT_AUTH_USERNAME_KEY = 'email'
    JWT_EXPIRATION_DELTA = timedelta(seconds=6000)
    SECRET_KEY = os.getenv('SECRET_KEY', 'change_me')
    LOG_LEVEL = os.getenv('LOG_LEVEL', logging.DEBUG)


class DevelopmentConfig(Config):
    """ Development configuration. """
    DEBUG = True


class TestingConfig(Config):
    """ Testing configuration used in unit tests. """
    DEBUG = True
    TESTING = True

    SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/test.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False


app_config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
}
