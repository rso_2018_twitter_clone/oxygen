from unittest import TestCase

from oxygen.models import User


class UsersTestCase(TestCase):
    def test_check_correct_password(self):
        user = User(password="pass")

        self.assertTrue(user.check_password("pass"))

    def test_check_incorrect_password(self):
        user = User(password="pass")

        self.assertFalse(user.check_password("incorrect"))
