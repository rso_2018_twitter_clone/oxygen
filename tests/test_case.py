import unittest

from config import app_config
from oxygen import app, db


def get_app_client():
    app.config.from_object(app_config['testing'])
    # creates a test client
    client = app.test_client()
    # propagate the exceptions to the test client
    client.testing = True
    return client


class TestCase(unittest.TestCase):
    def setUp(self):
        db.create_all()
        self.app = get_app_client()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
