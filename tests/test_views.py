import json

from oxygen import app
from oxygen.models import User
from tests.test_case import TestCase


class HealthcheckTestCase(TestCase):
    def setUp(self):
        # creates a test client
        self.app = app.test_client()
        # propagate the exceptions to the test client
        self.app.testing = True

    def test_ok(self):
        result = self.app.get('/health')

        self.assertEqual(result.status_code, 200)
        self.assertEqual(json.loads(result.data), {"alive": True})


class UsersTestCase(TestCase):

    def test_get_list_ok(self):
        users = [
            User(display_name="Megan Clark", password="command", email="megan.clark24@example.com"),
            User(display_name="Justin Ruiz", password="asdasd", email="justin.ruiz54@example.com"),
        ]
        User.save_all(*users)

        result = self.app.get('/users')

        self.assertEqual(result.status_code, 200)
        self.assertCountEqual(result.get_json(), [
            {
                'display_name': u.display_name,
                'id': u.id,
                'email': u.email,
                'create_date': u.create_date,
            } for u in users
        ])

    def test_list_search_ok(self):
        users = [
            User(display_name="Megan Clark", password="command", email="megan.clark24@example.com"),
            User(display_name="Justin Ruiz", password="asdasd", email="justin.ruiz54@example.com"),
        ]
        User.save_all(*users)

        result = self.app.get('/users?q=Megan')

        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.get_json(), [
            {
                'display_name': users[0].display_name,
                'id': users[0].id,
                'email': users[0].email,
                'create_date': users[0].create_date,
            }
        ])

    def test_get_ok(self):
        users = [
            User(display_name="Megan Clark", password="command", email="megan.clark24@example.com"),
            User(display_name="Justin Ruiz", password="asdasd", email="justin.ruiz54@example.com"),
        ]
        User.save_all(*users)
        user = users[0]

        result = self.app.get('/users/{0}'.format(user.id))

        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.json, {
            'display_name': user.display_name,
            'id': user.id,
            'email': user.email,
            'create_date': user.create_date,
        })

    def test_get_unknown_id(self):
        result = self.app.get('/users/1')

        self.assertEqual(result.status_code, 404)
        self.assertEqual(result.get_json(), {'error': 'Not found /users/1'})

    def test_post_ok(self):
        user_data = {'display_name': 'New user', 'password': 'pass', 'email': 'a@b.c'}

        result = self.app.post('/users', json=user_data)

        self.assertEqual(result.status_code, 200)
        self.assertEqual(json.loads(result.data)['display_name'], user_data['display_name'])
        self.assertEqual(json.loads(result.data)['id'], 1)
        self.assertEqual(json.loads(result.data)['email'], user_data['email'])

        u = User.query.first()
        self.assertEqual(user_data['display_name'], u.display_name)
        self.assertEqual(user_data['password'], 'pass')
        self.assertEqual(user_data['email'], u.email)

    def test_post_unique_error(self):
        users = [
            User(display_name="Megan Clark", password="command", email="a@b.c"),
        ]
        User.save_all(*users)
        user_data = {'display_name': 'New user', 'password': 'pass', 'email': 'a@b.c'}

        result = self.app.post('/users', json=user_data)

        self.assertEqual(result.status_code, 400)
        self.assertIn('error', result.json)


class AuthTestCase(TestCase):
    def test_get_token_ok(self):
        users = [
            User(display_name="Megan Clark", password="command", email="megan.clark24@example.com"),
            User(display_name="Justin Ruiz", password="asdasd", email="justin.ruiz54@example.com"),
        ]
        User.save_all(*users)

        result = self.app.post(
            '/auth',
            json={'email': users[0].email, 'password': 'command'},
        )

        self.assertEqual(result.status_code, 200)

        data = result.get_json()
        self.assertIn('access_token', data)

    def test_get_token_error(self):
        result = self.app.post(
            '/auth',
            json={'username': 'irrelevant', 'password': 'asdasd'},
        )

        self.assertEqual(result.status_code, 401)
